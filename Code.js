function onEdit(e) {
  // Ensure that the edit event object (e) is valid
  if (!e || !e.range || !e.value) {
    return;
  }

  // Get the edited sheet and range details
  const sheet = e.range.getSheet();
  const editedSheetName = sheet.getName();

  refreshSheetData(editedSheetName)

  const editedRow = e.range.getRow();
  const editedColumn = e.range.getColumn();

  // Check if the edit is relevant for the component system (you might want to restrict this to certain sheets or columns)
  if (isRelevantEdit(editedSheetName, editedRow, editedColumn)) {
    // Get the name of the component that was edited
    // Assuming the component name is in the first row of the edited column
    const componentName = sheet.getRange(1, editedColumn).getValue();

    // Fetch the component data from the sheet
    let componentData = fetchComponentData(componentName, editedSheetName);

    // Create a simplified component and inflate it
    let component = new Component(componentData);
    inflateComponent(component);

   Logger.log(JSON.stringify(component, null, 2));
    const price = upsertPrice(component);
    Logger.log(`Price: ${price}`);
    updateDataValidationsForInflatedSubcomponents(component);
  }
}

function findRowIndexForSubcomponentType(data, subcomponentType, subcomponentIndex) {
  // Find the row for the subcomponent type, considering multiple occurrences
  let count = 0;
  for (let i = 1; i < data.length; i++) { // Start from 1 to skip header row
    if (data[i][0] === subcomponentType) {
      if (count === subcomponentIndex) {
        return i + 1; // +1 for 1-indexed
      }
      count++;
    }
  }
  return -1; // Return -1 if not found
}


function isRelevantEdit(editedSheetName, editedRow, editedColumn) {
  // Add logic here to determine if the edit should trigger the component system
  // For example, you might want to exclude header rows, certain sheets, etc.
  


  return true;
}


function clearAllCache() {
  // Assuming you have an array of all cache keys
  const allCacheKeys = getAllCacheKeys(); // You need to implement this based on your system

  allCacheKeys.forEach(key => {
    cache.remove(key);
  });
}
