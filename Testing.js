const TEST_MEMORY_NAME = "Test Corsair Vengeance 16GB";
const TEST_BUILD_NAME = "Apps Script Test Build";

function checkMemoryCache() {
  const cachedData = cache.get(cachePrefixComponents + "Memory");

  if (cachedData) {
    const entities = JSON.parse(cachedData);
    // Find the specific entity with TEST_MEMORY_NAME
    const entity = entities.find(entity => entity.id === TEST_MEMORY_NAME);
    if (entity) {
      Logger.log(`Found in cache: ${JSON.stringify(entity)}`);
    } else {
      Logger.log('Test entity not found in cache');
    }
  } else {
    Logger.log('Cache is empty or data not found for Memory sheet');
  }
}

function testBuildEdit() {
  // Assuming "Builds" is the sheet name where builds are listed
  const buildSheet = activeSpreadsheet.getSheetByName("Build");

  // Find the row and column corresponding to the Memory component of a specific build
  // For demonstration, let's assume Memory is in the first row and the build is in the second column
  const memoryRow = findRowByName(buildSheet.getName(), "Memory");
  const buildColumn = 2; // Column for the specific build to test

  // Create a simulated edit event object
  let simulatedEditEvent = {
    range: buildSheet.getRange(memoryRow, buildColumn),
    value: TEST_MEMORY_NAME,
    source: activeSpreadsheet,
    user: Session.getActiveUser(),
    authMode: ScriptApp.AuthMode.LIMITED
  };

  // Call the onEdit function with the simulated event
  onEdit(simulatedEditEvent);
}

function testClusterEdit() {
  // Assuming "Clusters" is the sheet name where clusters are listed
  const clusterSheet = activeSpreadsheet.getSheetByName("Cluster");

  // Find the row and column corresponding to the Build component of a specific cluster
  // For demonstration, let's assume Build is in a specific row, and the cluster is in a specific column
  const buildRow = findRowByName(clusterSheet.getName(), "Build");
  const clusterColumn = 3; // Column for the specific cluster to test

  // Create a simulated edit event object
  let simulatedEditEvent = {
    range: clusterSheet.getRange(buildRow, clusterColumn),
    value: TEST_BUILD_NAME,
    source: activeSpreadsheet,
    user: Session.getActiveUser(),
    authMode: ScriptApp.AuthMode.LIMITED
  };

  // Call the onEdit function with the simulated event
  onEdit(simulatedEditEvent);
}