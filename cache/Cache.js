const cachePrefixComponents = "namespace_v2_"
const cache = CacheService.getDocumentCache()
const activeSpreadsheet = SpreadsheetApp.getActiveSpreadsheet()
const cachePrefixSheetData = "sheetData_" // Prefix for cache keys
const cachedSheetData = {}

// Function to fetch and cache sheet data
function refreshSheetData(sheetName) {
  const cacheKey = cachePrefixSheetData + sheetName
  const sheet = activeSpreadsheet.getSheetByName(sheetName)
  if (sheet) {
    const data = sheet.getDataRange().getValues()
    cache.put(cacheKey, JSON.stringify(data), 21600) // Cache for 6 hours
    cachedSheetData[sheetName] = data  // Update in-memory cache
    return data
  }
  return null // Return null if sheet not found
}

// Function to add a lazy-loaded getter
const addSheetDataGetter = (obj, sheetName) => {
  Object.defineProperty(obj, sheetName, {
    enumerable: true,
    configurable: true,
    get() {
      delete this[sheetName]  // Delete the getter
      return refreshSheetData(sheetName)
    },
  })
}

// Initialize lazy-loaded getters for each sheet
const initializeCachedSheetData = () => {
  const sheets = activeSpreadsheet.getSheets()
  sheets.forEach(sheet => addSheetDataGetter(cachedSheetData, sheet.getName()))
}

initializeCachedSheetData()