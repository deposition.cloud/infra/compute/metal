class CompatibilityFilter {
  constructor(inflatedComponent, inflatedParentComponent) {
    this.component = inflatedComponent;
    this.parentComponent = inflatedParentComponent;
    this.criteriaFunctions = [];
  }

  addCriteria(criteriaFunction) {
    this.criteriaFunctions.push(criteriaFunction);
    return this; // Allow chaining
  }

  getCompatibleOptionsNames() {
    let allOptions = this.getAllOptions(); // Retrieve all options for this component type
    let filteredOptions = this.criteriaFunctions.reduce((filteredOptions, criteria) => {
      return filteredOptions.filter(option => criteria(this.component, this.parentComponent, option));
    }, allOptions);

    // Extract the names from the filtered, fully inflated subcomponents
    return filteredOptions.map(option => option.Name);
  }

  getAllOptions() {
    // Retrieve component type from the inflated component
    const componentType = this.component["Type"];

    // Fetch all options for this component type
    return fetchAllOptionsForType(componentType);
  }
}

function fetchAllOptionsForType(componentType) {
  // Fetch data from the corresponding sheet based on componentType
  const sheet = activeSpreadsheet.getSheetByName(componentType);
  if (!sheet) return [];

  const data = cachedSheetData[sheet.getName()]
  let inflatedOptions = [];

  // Iterate over the first row starting from the second cell for component names
  for (let i = 1; i < data[0].length; i++) {
    let optionName = data[0][i]; // Component names are in the first row, excluding the first cell
    if (optionName) {
      // Fetch and inflate each component
      let componentData = fetchComponentData(optionName, componentType);
      if (componentData) {
        let component = new Component(componentData);
        inflateComponent(component, 1); // Inflate the component, but limit the depth for performance reasons
        inflatedOptions.push(component);
      }
    }
  }

  return inflatedOptions;
}