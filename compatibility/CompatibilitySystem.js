function isMemoryCompatibleWithMotherboard(memoryComponent, buildComponent, memoryOption) {
  const motherboardMemoryGeneration = buildComponent?.['Motherboard']?.['Memory Generation']
  
  if (!motherboardMemoryGeneration) {
    return true
  } else {
    return motherboardMemoryGeneration === memoryOption?.['Generation']
  }
}

function isMemoryCompatibleWithCpu(memoryComponent, buildComponent, memoryOption) {
  return true
}

function isMotherboardCompatibleWithCpu(motherboardComponent, buildComponent, motherboardOption) {
  const cpuSocket = buildComponent?.['CPU']?.['Socket']
  
  if (!cpuSocket) {
    return true
  } else {
    return cpuSocket === motherboardOption?.['Socket']
  }
}

function isCpuCompatibleWithMotherboard(cpuComponent, buildComponent, cpuOption) {
  const motherboardSocket = buildComponent?.['Motherboard']?.['Socket']

  if (!motherboardSocket) {
    return true
  } else {
    return motherboardSocket === cpuOption?.['Socket']
  }
}

function isAlwaysCompatible() {
  return true
}

const compatibilityFilters = {
  'Memory': (component, parent) => new CompatibilityFilter(component, parent)
    .addCriteria(isMemoryCompatibleWithMotherboard)
    .addCriteria(isMemoryCompatibleWithCpu),
  'Motherboard': (component, parent) => new CompatibilityFilter(component, parent)
    .addCriteria(isMotherboardCompatibleWithCpu),
  'CPU': (component, parent) => new CompatibilityFilter(component, parent)
    .addCriteria(isCpuCompatibleWithMotherboard),
  // ... other component types
};

function defaultFilter(component, parent) {
  return new CompatibilityFilter(component, parent)
    .addCriteria(isAlwaysCompatible)
}