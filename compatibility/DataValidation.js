const EXCLUDED_SUBCOMPONENT_KEYS = ['Name', 'Type', 'Price'];
const VALID_COMPONENT_TYPES = ['Build', 'Cluster'];

function updateDataValidationsForInflatedSubcomponents(component) {
  if (!VALID_COMPONENT_TYPES.includes(component.Type)) {
    return;
  }

  Object.keys(component).forEach(subcomponentKey => {
    if (!EXCLUDED_SUBCOMPONENT_KEYS.includes(subcomponentKey)) {
      if (Array.isArray(component[subcomponentKey])) {
        component[subcomponentKey].forEach((subcomponent, index) => updateDataValidationForSingleComponent(subcomponent, component, subcomponentKey, index));
      } else {
        updateDataValidationForSingleComponent(component[subcomponentKey], component, subcomponentKey, 0);
      }
    }
  });
}

function updateDataValidationForSingleComponent(inflatedSubcomponent, inflatedParentComponent, subcomponentKey, subcomponentIndex) {
  // Determine the type of the parent component
  let parentComponentType = inflatedParentComponent["Type"];

  // Get the sheet corresponding to the parent component type
  const sheet = activeSpreadsheet.getSheetByName(parentComponentType);
  if (sheet) {
    const data = cachedSheetData[sheet.getName()]

    // Find the column for the specific subcomponent instance
    let columnIndex = findColumnIndexForSubcomponentInstance(data[0], inflatedParentComponent['Name']);
    if (columnIndex > 0) {
      // Find the row for the subcomponent type
      let rowIndex = findRowIndexForSubcomponentType(data, subcomponentKey, subcomponentIndex);

      if (rowIndex > 0) {
        // Set data validation for the cell
        let cell = sheet.getRange(rowIndex, columnIndex);

        const originalColor = cell.getBackground();
        cell.setBackground("#FFFF00"); // Change cell color to indicate processing

        // Fetch compatible options for the subcomponent
        let filter = compatibilityFilters[subcomponentKey]
          ? compatibilityFilters[subcomponentKey](inflatedSubcomponent, inflatedParentComponent)
          : defaultFilter(inflatedSubcomponent, inflatedParentComponent);
        let compatibleOptions = filter ? filter.getCompatibleOptionsNames() : ["No compatible option available"];

        
        let validationRule = SpreadsheetApp.newDataValidation()
          .requireValueInList(compatibleOptions, true)
          .build();

        cell.setDataValidation(validationRule);
        
        // Reset the cell color to its original state
        cell.setBackground(originalColor);
        Logger.log(`Upserting data validations for ${subcomponentKey} at row ${rowIndex}, column ${columnIndex}`);
      }
    }
  }
}

function findColumnIndexForSubcomponentInstance(headers, componentName) {
  // Assuming component names are in the first row, starting from the second column
  return headers.indexOf(componentName) + 1; // +1 for 1-indexed
}