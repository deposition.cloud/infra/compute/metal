class Component {
  constructor(data) {
    // Dynamically add properties or sub-components to this instance
    Object.keys(data).forEach(key => {
      this[key] = data[key];
    });
  }

  // Method to update a property or sub-component
  update(key, value) {
    this[key] = value;
  }

  // Method to retrieve a property or sub-component
  get(key) {
    return this[key]
  }
}

function getFromCache(key) {
  const cachedData = cache.get(cachePrefixComponents + key);
  return cachedData ? JSON.parse(cachedData) : null;
}

function saveToCache(key, data) {
  const serializedData = JSON.stringify(data);
  cache.put(cachePrefixComponents + key, serializedData);
}