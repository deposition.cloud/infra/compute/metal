function fetchComponentData(componentName, componentType = null) {
  const sheetName = componentType || componentName;
  
  const values = cachedSheetData[sheetName];
  if (!values) {
    return null; // Return null if data not found or sheet not found
  }

  let columnIndex = componentType ? values[0].indexOf(componentName) : 0;

  if (columnIndex !== -1) {
    let componentData = { "Type": componentType };
    for (let i = 0; i < values.length; i++) {
      let key = values[i][0]; // Row header as key
      let value = values[i][columnIndex]; // Value from the specific column

      if (key && value) {
        if (componentData.hasOwnProperty(key)) {
          if (!Array.isArray(componentData[key])) {
            componentData[key] = [componentData[key]];
          }
          componentData[key].push(value);
        } else {
          componentData[key] = value;
        }
      }
    }
    return componentData;
  }
  return null;
}

function inflateComponent(component, depth) {
  Object.keys(component).forEach(key => {
    // Skip inflating simple properties like 'Name'
    if (key === 'Name') {
      return;
    }

    let value = component[key];

    // Check if the value is an array, indicating multiple sub-components
    if (Array.isArray(value)) {
      value.forEach((subComponentName, index) => {
        // Process each sub-component in the array
        inflateSubComponent(component, key, subComponentName, depth, index);
      });
    } else if (typeof value === 'string') {
      // Handle single component names
      inflateSubComponent(component, key, value, depth);
    }
  });
}

function inflateSubComponent(parentComponent, key, subComponentName, depth = 0, index = null) {
  let cachedSubComponent = getFromCache(subComponentName);

  if (cachedSubComponent && isFullyInflated(cachedSubComponent)) {
    if (index !== null) {
      parentComponent[key][index] = cachedSubComponent;
    } else {
      parentComponent[key] = cachedSubComponent;
    }
  } else {
    // Fetch data for the sub-component based on its name
    let subComponentData = fetchComponentData(subComponentName, key);
    if (subComponentData) {
      let subComponent = new Component(subComponentData);
      
      if (index !== null) {
        parentComponent[key][index] = subComponent;
      } else {
        parentComponent[key] = subComponent;
      }

      // Recursive call for further inflation
      if ( depth ) {
        inflateComponent(subComponent, depth - 1); 
      }
      saveToCache(subComponentName, subComponent);
    }
  }
}

function isFullyInflated(component) {
  // Iterate over each key in the component
  for (let key in component) {
    // Check if the key value is a string and a sheet exists with the same name as the key
    if (typeof component[key] === 'string' && sheetExists(key)) {
      // If both conditions are met, the component is not fully inflated
      return false;
    }
  }
  // If none of the keys meet the above condition, the component is fully inflated
  return true;
}

function sheetExists(sheetName) {
  // Check if a sheet with the given name exists in the spreadsheet
  const sheet = activeSpreadsheet.getSheetByName(sheetName);
  return sheet !== null;
}