const sheetConfigs = {};

(function initializeSheetConfigs() {
  const excludedSheets = ['Intro', 'Issues', 'Licence']; // Sheets to exclude

  // Fetch all sheets and exclude the specified ones
  activeSpreadsheet.getSheets().forEach(sheet => {
    const sheetName = sheet.getName();
    if (!excludedSheets.includes(sheetName)) {
      Object.defineProperty(sheetConfigs, sheetName, {
        enumerable: true,
        configurable: true,
        get: function() {
          delete this[sheetName];
          return (this[sheetName] = {
            sheet: sheet,
            get rowIndices() {
              delete this.rowIndices;
              return (this.rowIndices = buildRowIndexMap(sheet));
            },
          });
        },
      });
    }
  });
})()

function buildRowIndexMap(sheet) {
  const rowIndices = {};
  const firstColumnValues = sheet.getRange(1, 1, sheet.getLastRow()).getValues();
  firstColumnValues.forEach((value, index) => {
    if (value[0]) {
      rowIndices[value[0]] = index + 1; // +1 because row numbers are 1-indexed
    }
  });
  return rowIndices;
}

function findRowByName(sheetName, name) {
  const sheetConfig = sheetConfigs[sheetName];
  if (!sheetConfig) {
    throw new Error('Sheet not found in sheetConfigs');
  }

  return sheetConfig.rowIndices[name] || -1;
}