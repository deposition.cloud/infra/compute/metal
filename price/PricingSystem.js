function upsertPrice(component) {
  // Base case: If the component is a leaf node
  if (isLeafComponent(component)) {
    return parseFloat(component['Price']) || 0; // Parse the price, defaulting to 0 if not available
  }

  // Recursive case: Sum prices of all sub-components
  let totalPrice = 0;
  Object.keys(component).forEach(key => {
    if (key !== 'Name' && key !== 'Price') { // Skip 'Name' and 'Price' properties
      let subComponent = component[key];
      if (Array.isArray(subComponent)) {
        // Handle array of sub-components
        subComponent.forEach(item => totalPrice += upsertPrice(item));
      } else if (typeof subComponent === 'object') {
        // Handle single sub-component
        totalPrice += upsertPrice(subComponent);
      }
    }
  });

  // Update the price of the current component if it's not a leaf
  if (!isLeafComponent(component)) {
    component['Price'] = totalPrice;
    updatePriceInSheet(component, totalPrice)
  }

  return totalPrice;
}

function updatePriceInSheet(component, newPrice) {
  const sheet = activeSpreadsheet.getSheetByName(component["Type"]);
  if (sheet) {
    const values = cachedSheetData[sheet.getName()]

    // Find the column for the component
    let columnIndex = values[0].indexOf(component["Name"]) + 1; // +1 because Spreadsheet columns are 1-indexed
    if (columnIndex > 1) { // Avoid updating anything on row headings (first column)
      // Use findRowByName to find the row for the price
      let priceRow = findRowByName(sheet.getName(), 'Price');
      if (priceRow > 0) {
        // Update the cell with the new price
        sheet.getRange(priceRow, columnIndex).setValue(newPrice);
      }
    }
  }
}

function isLeafComponent(component) {
  // A leaf component is one that does not have any sub-components
  return typeof component !== 'object' || component === null || Array.isArray(component) || !hasSubComponents(component);
}

function hasSubComponents(component) {
  // Check if the component has any sub-components, including those in Arrays
  return Object.keys(component).some(key => 
    typeof component[key] === 'object' || Array.isArray(component[key])
  );
}